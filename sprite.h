#ifndef SPRITE_H
#define SPRITE_H

#include <SDL2/SDL.h>

enum sprite_ids {
	SPRITE_BORDER,
	SPRITE_BRICK,
	SPRITE_RUNNING_1,
	SPRITE_RUNNING_2,
	SPRITE_RUNNING_3,
	SPRITE_IDLE,
	SPRITE_SPIKE,
	SPRITE_WATER,
	SPRITE_WAVE,
	LENGTH_OF_SPRITES,
};

void draw_sprite(int id, SDL_Surface *screen, SDL_Rect *dst_rect);
void init_sprites();
void set_sprites_to_actual_size();

#endif /* SPRITE_H */
