#include <stdbool.h>

#include <SDL2/SDL_image.h>

SDL_Surface *
load_image_asset(const char *image_path) 
{
	SDL_Surface *image = IMG_Load(image_path);
	if (image == NULL)
	{
		printf("Unable to load image %s!\n", image_path);
		exit(EXIT_FAILURE);
	}
	return image;
}
