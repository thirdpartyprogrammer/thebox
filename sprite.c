#include <stdlib.h>

#include <SDL2/SDL.h>

#include "sprite.h"
#include "image.h"

SDL_Surface *spritesheet;
SDL_Rect sprites[LENGTH_OF_SPRITES] = {
	{ 2,   2,  64, 64 }, /* SPRITE_BORDER     0 0 */
	{ 138, 2,  64, 64 }, /* SPRITE_BRICK      2 0 */
	{ 206, 2,  64, 64 }, /* SPRITE_RUNNING_1  3 0 */
	{ 0,   70, 64, 64 }, /* SPRITE_RUNNING_2  0 1 */
	{ 70,  70, 64, 64 }, /* SPRITE_RUNNING_3  1 1 */
	{ 138, 70, 64, 64 }, /* SPRITE_IDLE       2 1 */
	{ 206, 70, 64, 64 }, /* SPRITE_SPIKE      3 1 */
	{ 2,   138, 64, 64 }, /* SPRITE_WATER      0 2 */
	{ 70,  138, 64, 64 }, /* SPRITE_WAVE       1 2 */
};

void draw_sprite(int id, SDL_Surface *screen, SDL_Rect *dst_rect)
{
	if (!spritesheet)
		spritesheet = load_image_asset("./assets/man/man.png");

	SDL_BlitSurface(spritesheet, &sprites[id], screen, dst_rect);
}
