#ifndef WINDOW_H
#define WINDOW_H

SDL_Window *init_window(const char *title, int width, int height);

SDL_Surface *get_window_surface(SDL_Window* window);

#endif /* WINDOW_H */
