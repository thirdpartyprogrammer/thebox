#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>

SDL_Surface *load_image_asset(const char *image_path);

#endif /* IMAGE_H */
