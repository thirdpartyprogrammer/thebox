#include <stdio.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "init_sdl.h"
#include "window.h"
#include "sprite.h"

SDL_Window *window;
SDL_Surface *screen;
SDL_Event event;
bool game_should_run;

void initialize();
void run_event_loop();
void render();
void disengage();

int
main()
{
	initialize();

	while (game_should_run)
	{
		run_event_loop();
		render();
	}

	disengage();
}

void
initialize()
{
	init_sdl_component(SDL_INIT_VIDEO);

	window = init_window("Stickman Game", 500, 500);
	screen = get_window_surface(window);
	game_should_run = true;
}

void
run_event_loop()
{
	while (SDL_PollEvent(&event) != 0)
	{
		if (event.type == SDL_QUIT)
			game_should_run = false;
	}
}

void
render()
{
	static SDL_Rect dst_rect = { 0, 0, 64, 64 };

	draw_sprite(SPRITE_IDLE, screen, &dst_rect);
	SDL_UpdateWindowSurface(window);
}

void
disengage()
{
	SDL_FreeSurface(screen);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
