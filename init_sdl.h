#ifndef INIT_SDL_H
#define INIT_SDL_H

#include <stdint.h>

void init_sdl_component(uint32_t init_flag);

#endif /* INIT_SDL_H */
