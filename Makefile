CC       = gcc
CFLAGS   = -g -Wall -Wextra -Werror -I/usr/include/SDL2 -D_REENTRANT
LDLIBS   = -lSDL2 -lSDL2_image -lm

game: main.o init_sdl.o window.o sprite.o image.o
	$(CC) $(LDFLAGS) main.o init_sdl.o window.o sprite.o image.o -o game $(LDLIBS)

main.o: main.c
	$(CC) -c $(CFLAGS) main.c -o main.o

init_sdl.o: init_sdl.c
	$(CC) -c $(CFLAGS) init_sdl.c -o init_sdl.o

window.o: window.c
	$(CC) -c $(CFLAGS) window.c -o window.o

image.o: image.c
	$(CC) -c $(CFLAGS) image.c -o image.o

sprite.o: sprite.c
	$(CC) -c $(CFLAGS) sprite.c -o sprite.o

clean:
	rm -f game *.o
