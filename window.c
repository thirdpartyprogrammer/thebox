#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "window.h"

SDL_Window *
init_window(const char *title, int width, int height)
{
	SDL_Window *window = SDL_CreateWindow(title,
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			width,
			height,
			SDL_WINDOW_SHOWN);
	return window;
}

SDL_Surface *
get_window_surface(SDL_Window* window)
{
	SDL_Surface *screen = SDL_GetWindowSurface(window);
	return screen;
}
