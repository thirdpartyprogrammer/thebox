#include <stdint.h>

#include <SDL2/SDL.h>

#include "init_sdl.h"

void
init_sdl_component(uint32_t init_flag)
{
	if (SDL_WasInit(init_flag))
		return;
	if (SDL_Init(init_flag) < 0)
	{
		printf("SDL init error: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}
}
